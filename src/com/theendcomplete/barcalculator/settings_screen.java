package com.theendcomplete.barcalculator;

import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class settings_screen extends Activity implements OnClickListener {
	SharedPreferences sPref;
	Button btnSaveSettings;

	TextView tvBeerPrice;
	EditText etBeerPrice;
	TextView tvDarkPrice;
	EditText etDarkPrice;
	TextView tvShotPrice;
	EditText etShotPrice;
	TextView tvCoctPrice;
	EditText etCoctPrice;
	TextView tvFoodPrice;
	EditText etFoodPrice;
	TextView tvSnackPrice;
	EditText etSnackPrice;

	final String BEER_PRICE = "beer_price";
	final String DARK_PRICE = "dark_price";
	final String SHOT_PRICE = "shot_price";
	final String COCT_PRICE = "coct_price";
	final String FOOD_PRICE = "food_price";
	final String SNACK_PRICE = "snack_price";

	final String PRICES = "prices";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		EasyTracker.getInstance().setContext(this);
		setContentView(R.layout.settings);
		btnSaveSettings = (Button) findViewById(R.id.btnSaveSettings);
		btnSaveSettings.setOnClickListener(this);

		tvBeerPrice = (TextView) findViewById(R.id.tvBeerPrice);
		etBeerPrice = (EditText) findViewById(R.id.etBeer);
		
		tvDarkPrice = (TextView) findViewById(R.id.tvDarkPrice);
		etDarkPrice = (EditText) findViewById(R.id.etDarkPrice);

		tvShotPrice = (TextView) findViewById(R.id.tvShotPrice);
		etShotPrice = (EditText) findViewById(R.id.etShotPrice);

		tvCoctPrice = (TextView) findViewById(R.id.tvCoctPrice);
		etCoctPrice = (EditText) findViewById(R.id.etCoctPrice);

		tvFoodPrice = (TextView) findViewById(R.id.tvFoodPrice);
		etFoodPrice = (EditText) findViewById(R.id.etFoodPrice);

		tvSnackPrice = (TextView) findViewById(R.id.tvSnackPrice);
		etSnackPrice = (EditText) findViewById(R.id.etSnackPrice);

		LoadPrice(etBeerPrice);
		LoadPrice(etDarkPrice);
		LoadPrice(etShotPrice);
		LoadPrice(etCoctPrice);
		LoadPrice(etFoodPrice);
		LoadPrice(etSnackPrice);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.btnSaveSettings:

			SavePrice(etBeerPrice);
			SavePrice(etDarkPrice);
			SavePrice(etShotPrice);
			SavePrice(etCoctPrice);
			SavePrice(etFoodPrice);
			SavePrice(etSnackPrice);
			finish();
			break;
		}

	}

	public void SavePrice(EditText et) {

		sPref = getSharedPreferences(PRICES, MODE_PRIVATE);
		Editor edBeer = sPref.edit();
		Editor edDark = sPref.edit();
		Editor edShot = sPref.edit();
		Editor edCoct = sPref.edit();
		Editor edFood = sPref.edit();
		Editor edSnack = sPref.edit();
		switch (et.getId()) {
		case R.id.etBeer:
			edBeer.putString(BEER_PRICE, etBeerPrice.getText().toString());
			edBeer.commit();
			break;
		case R.id.etDarkPrice:
			edDark.putString(DARK_PRICE, etDarkPrice.getText().toString());
			edDark.commit();
			break;
		case R.id.etShotPrice:
			edShot.putString(SHOT_PRICE, etShotPrice.getText().toString());
			edShot.commit();
			break;
		case R.id.etCoctPrice:
			edCoct.putString(COCT_PRICE, etCoctPrice.getText().toString());
			edCoct.commit();
			break;
		case R.id.etFoodPrice:
			edFood.putString(FOOD_PRICE, etFoodPrice.getText().toString());
			edFood.commit();
			break;
		case R.id.etSnackPrice:
			edSnack.putString(SNACK_PRICE, etSnackPrice.getText().toString());
			edSnack.commit();
			break;
		}

	}

	public void LoadPrice(EditText et) {
		String savedString = "";
		sPref = getSharedPreferences(PRICES, MODE_PRIVATE);
		switch (et.getId()) {
		case R.id.etBeer:
			savedString = sPref.getString(BEER_PRICE, "");
			etBeerPrice.setText(savedString);
			break;
		case R.id.etDarkPrice:
			savedString = sPref.getString(DARK_PRICE, "");
			etDarkPrice.setText(savedString);
			break;
		case R.id.etShotPrice:
			savedString = sPref.getString(SHOT_PRICE, "");
			etShotPrice.setText(savedString);
			break;
		case R.id.etCoctPrice:
			savedString = sPref.getString(COCT_PRICE, "");
			etCoctPrice.setText(savedString);
			break;
		case R.id.etFoodPrice:
			savedString = sPref.getString(FOOD_PRICE, "");
			etFoodPrice.setText(savedString);
			break;
		case R.id.etSnackPrice:
			savedString = sPref.getString(SNACK_PRICE, "");
			etSnackPrice.setText(savedString);
			break;

		}

	}

	float ConvertEtToFloat(String et) {
		String value = et;
		if (et.trim().length() != 0) {
			float floatValue = Float.parseFloat(value);
			return floatValue;

		} else {
			return 0;
		}

	}
	
}
