package com.theendcomplete.barcalculator;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

public class Alcohol extends Activity implements OnClickListener {
	SharedPreferences sPref;
	// Button btnSaveAlco = (Button) findViewById(R.id.btnSaveAlco);
	// EditText etWeight = (EditText) findViewById(R.id.etWeight);
	// RadioGroup rgAlco = (RadioGroup) findViewById(R.id.rgAlco);
	Button btnSaveAlco;
	EditText etWeight;
	RadioGroup rgAlco;

	final String SEX = "sex";
	final String WEIGHT = "weight";
	final String ALCO = "alco";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// EasyTracker.getInstance().setContext(this);
		setContentView(R.layout.alcohol);
		btnSaveAlco = (Button) findViewById(R.id.btnSaveAlco);
		etWeight = (EditText) findViewById(R.id.etWeight);
		btnSaveAlco.setOnClickListener(this);
		rgAlco = (RadioGroup) findViewById(R.id.rgAlco);
		 LoadData ();
	}
public void LoadData(){
//	String savedString = "";
	sPref = getSharedPreferences(ALCO, MODE_PRIVATE);
	
		String savedSex = sPref.getString(SEX, "");
		if (savedSex=="0.70")
		{
			rgAlco.check(R.id.rMale);
		}
		else if (savedSex=="0.60")
		{
			rgAlco.check(R.id.rFemale);
		}
		
		
//		etBeerPrice.setText(savedString);
		
		String savedWeight = sPref.getString(WEIGHT, "");
		etWeight.setText(savedWeight);
		
}
	public void SaveData() {

		sPref = getSharedPreferences(ALCO, MODE_PRIVATE);
		Editor edSex = sPref.edit();
		Editor edWeight = sPref.edit();
		switch (rgAlco.getCheckedRadioButtonId()) {
		case R.id.rMale: {
			edSex.putString(SEX, "0.70");
			edSex.commit();
			break;
		}
		case R.id.rFemale: {
			edSex.putString(SEX, "0.60");
			edSex.commit();
			break;
		}
		}
		edWeight.putString(WEIGHT, etWeight.getText().toString());
		edWeight.commit();
		finish();
	}

	@Override
	public void onClick(View v) {
		SaveData();
	}
}
