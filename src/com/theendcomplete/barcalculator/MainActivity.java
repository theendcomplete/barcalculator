package com.theendcomplete.barcalculator;

import java.sql.Date;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

//import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	// GoogleAnalytics tracker;
	SharedPreferences sPref;
	Button btnBeer;
	TextView tvBeer;
	float bCount = 0;
	float beerPrice = 0;

	Button btnDark;
	TextView tvDark;
	float dCount = 0;
	float darkPrice = 0;

	Button btnShot;
	TextView tvShot;
	float sCount = 0;
	float shotPrice = 0;
	Button btnCoct;
	TextView tvCoct;
	float cCount = 0;
	float coctPrice = 0;
	Button btnFood;
	TextView tvFood;
	float fCount = 0;
	float fPrice = 0;
	Button btnSnack;
	TextView tvSnack;
	float snCount = 0;
	float snPrice = 0;
	TextView tvAlco;

	float sex = 0;
	float weight = 0;
	float promille = 0;
	float beer_alco = (float) 17.73;
	float dark_alco = 18;
	float shot_alco = 16;
	float coctail_alco = 25;

	final String PRICES = "prices";
	final String COUNTS = "counts";

	final String SEX = "sex";
	final String WEIGHT = "weight";
	final String ALCO = "alco";
	Long startTime;
	Date time;

	float Sum = 0;
	TextView tvSum;
	// ������� ��� �������� �������� ���
	final String BEER_PRICE = "beer_price";
	final String DARK_PRICE = "dark_price";
	final String SHOT_PRICE = "shot_price";
	final String COCT_PRICE = "coct_price";
	final String FOOD_PRICE = "food_price";
	final String SNACK_PRICE = "snack_price";

	final String BEER_COUNT = "beer_count";
	final String DARK_COUNT = "dark_count";
	final String SHOT_COUNT = "shot_count";
	final String COCT_COUNT = "coct_count";
	final String FOOD_COUNT = "food_count";
	final String SNACK_COUNT = "snack_count";
	final String START_TIME = "starttime";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		EasyTracker.getInstance().setContext(this);

		setContentView(R.layout.activity_main);
		btnBeer = (Button) findViewById(R.id.btnBeer);
		tvBeer = (TextView) findViewById(R.id.tvBeer);
		btnBeer.setOnClickListener(this);

		btnDark = (Button) findViewById(R.id.btnDark);
		tvDark = (TextView) findViewById(R.id.tvDark);
		btnDark.setOnClickListener(this);

		btnShot = (Button) findViewById(R.id.btnShot);
		tvShot = (TextView) findViewById(R.id.tvShot);
		btnShot.setOnClickListener(this);

		btnCoct = (Button) findViewById(R.id.btnCoct);
		tvCoct = (TextView) findViewById(R.id.tvCoct);
		btnCoct.setOnClickListener(this);

		btnFood = (Button) findViewById(R.id.btnFood);
		tvFood = (TextView) findViewById(R.id.tvFood);
		btnFood.setOnClickListener(this);

		btnSnack = (Button) findViewById(R.id.btnSnack);
		tvSnack = (TextView) findViewById(R.id.tvSnack);
		btnSnack.setOnClickListener(this);

		tvAlco = (TextView) findViewById(R.id.tvAlco);

		tvSum = (TextView) findViewById(R.id.tvSum);
		LoadPrice();
		LoadCounts();
		UpdateSum();

	}

	void ClearCounts() {
		bCount = 0;
		tvBeer.setText(R.string.blank);

		dCount = 0;
		tvDark.setText(R.string.blank);

		sCount = 0;
		tvShot.setText(R.string.blank);

		cCount = 0;
		tvCoct.setText(R.string.blank);

		fCount = 0;
		tvFood.setText(R.string.blank);

		snCount = 0;
		tvSnack.setText(R.string.blank);

		tvSum.setText(R.string.blank);
		UpdateSum();
		CountAlco();

	}

	@Override
	protected void onStart() {
		if (getResources().getBoolean(R.bool.analytics_enabled)) {
			EasyTracker.getInstance().activityStart(this);
		}
		super.onStart();
	}

	@Override
	protected void onResume() {

		super.onResume();
		LoadPrice();
		LoadAlco();
		UpdateSum();
		CountAlco();

	}

	@Override
	protected void onDestroy() {

		super.onDestroy();

		SaveCounts();
		EasyTracker.getInstance().activityStop(this);
	}

	void SaveCounts() {
		sPref = getSharedPreferences(COUNTS, MODE_PRIVATE);
		Editor edBeer = sPref.edit();
		Editor edDark = sPref.edit();
		Editor edShot = sPref.edit();
		Editor edCoct = sPref.edit();
		Editor edFood = sPref.edit();
		Editor edSnack = sPref.edit();

		edBeer.putString(BEER_COUNT, String.valueOf(bCount));
		edBeer.commit();
		edDark.putString(DARK_COUNT, String.valueOf(dCount));
		edDark.commit();
		edShot.putString(SHOT_COUNT, String.valueOf(sCount));
		edShot.commit();
		edCoct.putString(COCT_COUNT, String.valueOf(cCount));
		edCoct.commit();
		edFood.putString(FOOD_COUNT, String.valueOf(fCount));
		edFood.commit();
		edSnack.putString(SNACK_COUNT, String.valueOf(snCount));
		edSnack.commit();

	}

	public void LoadCounts() {
		sPref = getSharedPreferences(COUNTS, MODE_PRIVATE);
		String savedBeer = sPref.getString(BEER_COUNT, "");
		bCount = ConvertStringTofloat(savedBeer);
		if ((bCount == 0)) {
			tvBeer.setText(getString(R.string.blank));

		} else {
			tvBeer.setText(" X " + savedBeer + "  ");

		}
		String savedDark = sPref.getString(DARK_COUNT, "");
		dCount = ConvertStringTofloat(savedDark);
		if ((dCount == 0)) {
			tvDark.setText(getString(R.string.blank));

		} else {
			tvDark.setText(" X " + savedDark + "  ");
		}
		String savedShot = sPref.getString(SHOT_COUNT, "");
		sCount = ConvertStringTofloat(savedShot);
		if ((sCount == 0)) {
			tvShot.setText(getString(R.string.blank));

		} else {
			tvShot.setText(" X " + savedShot + "  ");

		}
		String savedCoct = sPref.getString(COCT_COUNT, "");
		cCount = ConvertStringTofloat(savedCoct);
		if ((cCount == 0)) {
			tvCoct.setText(getString(R.string.blank));

		} else {
			tvCoct.setText(" X " + savedCoct + "  ");

		}
		String savedFood = sPref.getString(FOOD_COUNT, "");
		fCount = ConvertStringTofloat(savedFood);
		if ((fCount == 0)) {
			tvFood.setText(getString(R.string.blank));

		} else {
			tvFood.setText(" X " + savedFood + "  ");

		}
		String savedSnack = sPref.getString(SNACK_COUNT, "");
		snCount = ConvertStringTofloat(savedSnack);
		if ((snCount == 0)) {
			tvSnack.setText(getString(R.string.blank));

		} else {
			tvSnack.setText(" X " + savedSnack + "  ");

		}
		CountAlco();
	}

	public void StartTime() {
		startTime = InitDate();
	}

	public Long InitDate() {
//		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		time = new Date(System.currentTimeMillis());
		return (time.getTime());
	}

	public void CountAlco() {
		if (startTime==null) {
			StartTime();
		}

		Long currentTime = InitDate();
		Long diff = (currentTime - startTime); //������� � �����
//		Long diff = (currentTime - startTime)/100/60/60; //������� � �����
		float floatDiff = (float) diff;
		floatDiff = floatDiff/1000/60;

		
		if ((weight != 0) && (sex != 0)) {
			promille = ((beer_alco * bCount) + (dark_alco * dCount)
					+ (shot_alco * sCount) + (coctail_alco * cCount))
					/ (weight * sex)*(150*floatDiff);
			
			promille = (int)(promille *1000);
			promille = promille /1000;
			promille= (float) promille;
			
		}
		if (promille != 0) {

			tvAlco.setText(getString(R.string.BAC) + " "
					+ Float.toString(promille));
		}
		if (promille == 0) {
			tvAlco.setText(getString(R.string.blank));
		}

	}

	public void LoadAlco() {
		sPref = getSharedPreferences(ALCO, MODE_PRIVATE);
		String savedSex = sPref.getString(SEX, "");
		sex = ConvertStringTofloat(savedSex);

		String savedWeight = sPref.getString(WEIGHT, "");
		weight = ConvertStringTofloat(savedWeight);
	}

	public void LoadPrice() {
		sPref = getSharedPreferences(PRICES, MODE_PRIVATE);
		String savedBeer = sPref.getString(BEER_PRICE, "");
		beerPrice = ConvertStringTofloat(savedBeer);
		String savedDark = sPref.getString(DARK_PRICE, "");
		darkPrice = ConvertStringTofloat(savedDark);
		String savedShot = sPref.getString(SHOT_PRICE, "");
		shotPrice = ConvertStringTofloat(savedShot);
		String savedCoct = sPref.getString(COCT_PRICE, "");
		coctPrice = ConvertStringTofloat(savedCoct);
		String savedFood = sPref.getString(FOOD_PRICE, "");
		fPrice = ConvertStringTofloat(savedFood);
		String savedSnack = sPref.getString(SNACK_PRICE, "");
		snPrice = ConvertStringTofloat(savedSnack);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	void UpdateSum() {
		Sum = bCount * beerPrice + dCount * darkPrice + sCount * shotPrice
				+ cCount * coctPrice + fCount * fPrice + snCount * snPrice;
		if (Sum > 0) {
			tvSum.setText(getString(R.string.summary) + "  " + Sum + "  ");
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent = new Intent(this, settings_screen.class);
			startActivity(intent);
			break;
		case R.id.clear_main:
			ClearCounts();
			SaveCounts();
			UpdateSum();
			break;
		case R.id.alcocalc:
			Intent intent_alc = new Intent(this, Alcohol.class);
			startActivity(intent_alc);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if (bCount == 0 || dCount == 0 || sCount == 0 || cCount == 0) {
			StartTime();
		}
		switch (v.getId()) {
		case R.id.btnBeer:
			bCount = bCount + 1;
			tvBeer.setText(" X " + bCount + "  ");

			break;
		case R.id.btnDark:
			dCount = dCount + 1;
			tvDark.setText(" X " + dCount + "  ");
			break;
		case R.id.btnShot:
			sCount = sCount + 1;
			tvShot.setText(" X " + sCount + "  ");
			break;

		case R.id.btnCoct:
			cCount = cCount + 1;
			tvCoct.setText(" X " + cCount + "  ");
			break;

		case R.id.btnFood:
			fCount = fCount + 1;
			tvFood.setText(" X " + fCount + "  ");
			break;

		case R.id.btnSnack:
			snCount = snCount + 1;
			tvSnack.setText(" X " + snCount + "  ");
			break;
		}
		CountAlco();
		UpdateSum();

	}

	float ConvertStringTofloat(String str) {
		String value = str;
		if (value.trim().length() != 0) {
			float floatValue = Float.parseFloat(value);
			return floatValue;

		} else {
			return 0;

		}

	}

}
